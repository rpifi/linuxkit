FROM golang:1.12.5-alpine3.9 AS build

RUN apk add --no-cache \
    gcc \
    git \
    musl-dev \
 && mkdir -p /go/src/github.com/linuxkit/linuxkit \
 && cd /go/src/github.com/linuxkit/linuxkit \
 && git init \
 && git remote add origin https://github.com/linuxkit/linuxkit.git \
 && git fetch --depth 1 origin 17359f2ed1575db4a47fc2de2ac1f3936e661e5b \
 && git checkout FETCH_HEAD
WORKDIR /go/src/github.com/linuxkit/linuxkit/src/cmd/linuxkit
RUN go get 


FROM docker:18.09.6

COPY --from=build /go/bin/linuxkit /usr/bin/linuxkit

ENTRYPOINT ["/usr/bin/linuxkit"]
